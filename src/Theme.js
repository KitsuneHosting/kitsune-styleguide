import React from 'react'

class Theme extends React.Component {
  render () {
    return (
      <div>
        <div className="container">
          <div className="page-header" id="banner">
            <div className="row">
              <div className="col-lg-8 col-md-7 col-sm-6">
                <h1>Cosmo</h1>
                <p className="lead">An ode to Metro</p>
              </div>
              <div className="col-lg-4 col-md-5 col-sm-6">
                <div className="sponsor">
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-4">
                <div className="list-group table-of-contents">
                  <a className="list-group-item" href="#navbar">Navbar</a>
                  <a className="list-group-item" href="#buttons">Buttons</a>
                  <a className="list-group-item" href="#typography">Typography</a>
                  <a className="list-group-item" href="#tables">Tables</a>
                  <a className="list-group-item" href="#forms">Forms</a>
                  <a className="list-group-item" href="#navs">Navs</a>
                  <a className="list-group-item" href="#indicators">Indicators</a>
                  <a className="list-group-item" href="#progress-bars">Progress bars</a>
                  <a className="list-group-item" href="#containers">Containers</a>
                  <a className="list-group-item" href="#dialogs">Dialogs</a>
                </div>
              </div>
            </div>
          </div>
          {/* Navbar
           ================================================== */}
          <div className="bs-docs-section clearfix">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="navbar">Navbar</h1>
                </div>
                <div className="bs-component">
                  <nav className="navbar navbar-default">
                    <div className="container-fluid">
                      <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span className="sr-only">Toggle navigation</span>
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                        </button>
                        <a className="navbar-brand" href="#">Brand</a>
                      </div>
                      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                          <li className="active"><a href="#">Link <span className="sr-only">(current)</span></a></li>
                          <li><a href="#">Link</a></li>
                          <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span className="caret" /></a>
                            <ul className="dropdown-menu" role="menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li className="divider" />
                              <li><a href="#">Separated link</a></li>
                              <li className="divider" />
                              <li><a href="#">One more separated link</a></li>
                            </ul>
                          </li>
                        </ul>
                        <form className="navbar-form navbar-left" role="search">
                          <div className="form-group">
                            <input type="text" className="form-control" placeholder="Search" />
                          </div>
                          <button type="submit" className="btn btn-default">Submit</button>
                        </form>
                        <ul className="nav navbar-nav navbar-right">
                          <li><a href="#">Link</a></li>
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>
                <div className="bs-component">
                  <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                      <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                          <span className="sr-only">Toggle navigation</span>
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                        </button>
                        <a className="navbar-brand" href="#">Brand</a>
                      </div>
                      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <ul className="nav navbar-nav">
                          <li className="active"><a href="#">Link <span className="sr-only">(current)</span></a></li>
                          <li><a href="#">Link</a></li>
                          <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span className="caret" /></a>
                            <ul className="dropdown-menu" role="menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li className="divider" />
                              <li><a href="#">Separated link</a></li>
                              <li className="divider" />
                              <li><a href="#">One more separated link</a></li>
                            </ul>
                          </li>
                        </ul>
                        <form className="navbar-form navbar-left" role="search">
                          <div className="form-group">
                            <input type="text" className="form-control" placeholder="Search" />
                          </div>
                          <button type="submit" className="btn btn-default">Submit</button>
                        </form>
                        <ul className="nav navbar-nav navbar-right">
                          <li><a href="#">Link</a></li>
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>{/* /example */}
              </div>
            </div>
          </div>
          {/* Buttons
           ================================================== */}
          <div className="bs-docs-section">
            <div className="page-header">
              <div className="row">
                <div className="col-lg-12">
                  <h1 id="buttons">Buttons</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-7">
                <p className="bs-component">
                  <a href="#" className="btn btn-default">Default</a>
                  <a href="#" className="btn btn-primary">Primary</a>
                  <a href="#" className="btn btn-success">Success</a>
                  <a href="#" className="btn btn-info">Info</a>
                  <a href="#" className="btn btn-warning">Warning</a>
                  <a href="#" className="btn btn-danger">Danger</a>
                  <a href="#" className="btn btn-link">Link</a>
                </p>
                <p className="bs-component">
                  <a href="#" className="btn btn-default disabled">Default</a>
                  <a href="#" className="btn btn-primary disabled">Primary</a>
                  <a href="#" className="btn btn-success disabled">Success</a>
                  <a href="#" className="btn btn-info disabled">Info</a>
                  <a href="#" className="btn btn-warning disabled">Warning</a>
                  <a href="#" className="btn btn-danger disabled">Danger</a>
                  <a href="#" className="btn btn-link disabled">Link</a>
                </p>
                <div style={{marginBottom: 15}}>
                  <div className="btn-toolbar bs-component" style={{margin: 0}}>
                    <div className="btn-group">
                      <a href="#" className="btn btn-default">Default</a>
                      <a href="#" className="btn btn-default dropdown-toggle" data-toggle="dropdown"><span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-primary">Primary</a>
                      <a href="#" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-success">Success</a>
                      <a href="#" className="btn btn-success dropdown-toggle" data-toggle="dropdown"><span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-info">Info</a>
                      <a href="#" className="btn btn-info dropdown-toggle" data-toggle="dropdown"><span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-warning">Warning</a>
                      <a href="#" className="btn btn-warning dropdown-toggle" data-toggle="dropdown"><span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <p className="bs-component">
                  <a href="#" className="btn btn-primary btn-lg">Large button</a>
                  <a href="#" className="btn btn-primary">Default button</a>
                  <a href="#" className="btn btn-primary btn-sm">Small button</a>
                  <a href="#" className="btn btn-primary btn-xs">Mini button</a>
                </p>
              </div>
              <div className="col-lg-5">
                <p className="bs-component">
                  <a href="#" className="btn btn-default btn-lg btn-block">Block level button</a>
                </p>
                <div className="bs-component" style={{marginBottom: 15}}>
                  <div className="btn-group btn-group-justified">
                    <a href="#" className="btn btn-default">Left</a>
                    <a href="#" className="btn btn-default">Middle</a>
                    <a href="#" className="btn btn-default">Right</a>
                  </div>
                </div>
                <div className="bs-component" style={{marginBottom: 15}}>
                  <div className="btn-toolbar">
                    <div className="btn-group">
                      <a href="#" className="btn btn-default">1</a>
                      <a href="#" className="btn btn-default">2</a>
                      <a href="#" className="btn btn-default">3</a>
                      <a href="#" className="btn btn-default">4</a>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-default">5</a>
                      <a href="#" className="btn btn-default">6</a>
                      <a href="#" className="btn btn-default">7</a>
                    </div>
                    <div className="btn-group">
                      <a href="#" className="btn btn-default">8</a>
                      <div className="btn-group">
                        <a href="#" className="btn btn-default dropdown-toggle" data-toggle="dropdown">
                          Dropdown
                          <span className="caret" />
                        </a>
                        <ul className="dropdown-menu">
                          <li><a href="#">Dropdown link</a></li>
                          <li><a href="#">Dropdown link</a></li>
                          <li><a href="#">Dropdown link</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bs-component">
                  <div className="btn-group-vertical">
                    <a href="#" className="btn btn-default">Button</a>
                    <a href="#" className="btn btn-default">Button</a>
                    <a href="#" className="btn btn-default">Button</a>
                    <a href="#" className="btn btn-default">Button</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Typography
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="typography">Typography</h1>
                </div>
              </div>
            </div>
            {/* Headings */}
            <div className="row">
              <div className="col-lg-4">
                <div className="bs-component">
                  <h1>Heading 1</h1>
                  <h2>Heading 2</h2>
                  <h3>Heading 3</h3>
                  <h4>Heading 4</h4>
                  <h5>Heading 5</h5>
                  <h6>Heading 6</h6>
                  <p className="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <h2>Example body text</h2>
                  <p>Nullam quis risus eget <a href="#">urna mollis ornare</a> vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                  <p><small>This line of text is meant to be treated as fine print.</small></p>
                  <p>The following snippet of text is <strong>rendered as bold text</strong>.</p>
                  <p>The following snippet of text is <em>rendered as italicized text</em>.</p>
                  <p>An abbreviation of the word attribute is <abbr title="attribute">attr</abbr>.</p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <h2>Emphasis classes</h2>
                  <p className="text-muted">Fusce dapibus, tellus ac cursus commodo, tortor mauris nibh.</p>
                  <p className="text-primary">Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                  <p className="text-warning">Etiam porta sem malesuada magna mollis euismod.</p>
                  <p className="text-danger">Donec ullamcorper nulla non metus auctor fringilla.</p>
                  <p className="text-success">Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                  <p className="text-info">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
                </div>
              </div>
            </div>
            {/* Blockquotes */}
            <div className="row">
              <div className="col-lg-12">
                <h2 id="type-blockquotes">Blockquotes</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6">
                <div className="bs-component">
                  <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                  </blockquote>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="bs-component">
                  <blockquote className="blockquote-reverse">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          {/* Tables
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="tables">Tables</h1>
                </div>
                <div className="bs-component">
                  <table className="table table-striped table-hover ">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr className="info">
                      <td>3</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr className="success">
                      <td>4</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr className="danger">
                      <td>5</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr className="warning">
                      <td>6</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    <tr className="active">
                      <td>7</td>
                      <td>Column content</td>
                      <td>Column content</td>
                      <td>Column content</td>
                    </tr>
                    </tbody>
                  </table>
                </div>{/* /example */}
              </div>
            </div>
          </div>
          {/* Forms
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="forms">Forms</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6">
                <div className="well bs-component">
                  <form className="form-horizontal">
                    <fieldset>
                      <legend>Legend</legend>
                      <div className="form-group">
                        <label htmlFor="inputEmail" className="col-lg-2 control-label">Email</label>
                        <div className="col-lg-10">
                          <input type="text" className="form-control" id="inputEmail" placeholder="Email" />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="inputPassword" className="col-lg-2 control-label">Password</label>
                        <div className="col-lg-10">
                          <input type="password" className="form-control" id="inputPassword" placeholder="Password" />
                          <div className="checkbox">
                            <label>
                              <input type="checkbox" /> Checkbox
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="textArea" className="col-lg-2 control-label">Textarea</label>
                        <div className="col-lg-10">
                          <textarea className="form-control" rows={3} id="textArea" defaultValue={""} />
                          <span className="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="col-lg-2 control-label">Radios</label>
                        <div className="col-lg-10">
                          <div className="radio">
                            <label>
                              <input type="radio" name="optionsRadios" id="optionsRadios1" defaultValue="option1" defaultChecked />
                              Option one is this
                            </label>
                          </div>
                          <div className="radio">
                            <label>
                              <input type="radio" name="optionsRadios" id="optionsRadios2" defaultValue="option2" />
                              Option two can be something else
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="select" className="col-lg-2 control-label">Selects</label>
                        <div className="col-lg-10">
                          <select className="form-control" id="select">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </select>
                          <br />
                          <select multiple className="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </select>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-lg-10 col-lg-offset-2">
                          <button type="reset" className="btn btn-default">Cancel</button>
                          <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
              <div className="col-lg-4 col-lg-offset-1">
                <form className="bs-component">
                  <div className="form-group">
                    <label className="control-label" htmlFor="focusedInput">Focused input</label>
                    <input className="form-control" id="focusedInput" type="text" defaultValue="This is focused..." />
                  </div>
                  <div className="form-group">
                    <label className="control-label" htmlFor="disabledInput">Disabled input</label>
                    <input className="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled />
                  </div>
                  <div className="form-group has-warning">
                    <label className="control-label" htmlFor="inputWarning">Input warning</label>
                    <input type="text" className="form-control" id="inputWarning" />
                  </div>
                  <div className="form-group has-error">
                    <label className="control-label" htmlFor="inputError">Input error</label>
                    <input type="text" className="form-control" id="inputError" />
                  </div>
                  <div className="form-group has-success">
                    <label className="control-label" htmlFor="inputSuccess">Input success</label>
                    <input type="text" className="form-control" id="inputSuccess" />
                  </div>
                  <div className="form-group">
                    <label className="control-label" htmlFor="inputLarge">Large input</label>
                    <input className="form-control input-lg" type="text" id="inputLarge" />
                  </div>
                  <div className="form-group">
                    <label className="control-label" htmlFor="inputDefault">Default input</label>
                    <input type="text" className="form-control" id="inputDefault" />
                  </div>
                  <div className="form-group">
                    <label className="control-label" htmlFor="inputSmall">Small input</label>
                    <input className="form-control input-sm" type="text" id="inputSmall" />
                  </div>
                  <div className="form-group">
                    <label className="control-label">Input addons</label>
                    <div className="input-group">
                      <span className="input-group-addon">$</span>
                      <input type="text" className="form-control" />
                      <span className="input-group-btn">
                      <button className="btn btn-default" type="button">Button</button>
                    </span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          {/* Navs
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="navs">Navs</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <h2 id="nav-tabs">Tabs</h2>
                <div className="bs-component">
                  <ul className="nav nav-tabs">
                    <li className="active"><a href="#home" data-toggle="tab">Home</a></li>
                    <li><a href="#profile" data-toggle="tab">Profile</a></li>
                    <li className="disabled"><a>Disabled</a></li>
                    <li className="dropdown">
                      <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                        Dropdown <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li><a href="#dropdown1" data-toggle="tab">Action</a></li>
                        <li className="divider" />
                        <li><a href="#dropdown2" data-toggle="tab">Another action</a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="myTabContent" className="tab-content">
                    <div className="tab-pane fade active in" id="home">
                      <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                    </div>
                    <div className="tab-pane fade" id="profile">
                      <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
                    </div>
                    <div className="tab-pane fade" id="dropdown1">
                      <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork.</p>
                    </div>
                    <div className="tab-pane fade" id="dropdown2">
                      <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <h2 id="nav-pills">Pills</h2>
                <div className="bs-component">
                  <ul className="nav nav-pills">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Profile</a></li>
                    <li className="disabled"><a href="#">Disabled</a></li>
                    <li className="dropdown">
                      <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                        Dropdown <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <br />
                <div className="bs-component">
                  <ul className="nav nav-pills nav-stacked">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Profile</a></li>
                    <li className="disabled"><a href="#">Disabled</a></li>
                    <li className="dropdown">
                      <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                        Dropdown <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider" />
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
                <h2 id="nav-breadcrumbs">Breadcrumbs</h2>
                <div className="bs-component">
                  <ul className="breadcrumb">
                    <li className="active">Home</li>
                  </ul>
                  <ul className="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li className="active">Library</li>
                  </ul>
                  <ul className="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Library</a></li>
                    <li className="active">Data</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <h2 id="pagination">Pagination</h2>
                <div className="bs-component">
                  <ul className="pagination">
                    <li className="disabled"><a href="#">«</a></li>
                    <li className="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                  <ul className="pagination pagination-lg">
                    <li className="disabled"><a href="#">«</a></li>
                    <li className="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                  <ul className="pagination pagination-sm">
                    <li className="disabled"><a href="#">«</a></li>
                    <li className="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
                <h2 id="pager">Pager</h2>
                <div className="bs-component">
                  <ul className="pager">
                    <li><a href="#">Previous</a></li>
                    <li><a href="#">Next</a></li>
                  </ul>
                  <ul className="pager">
                    <li className="previous disabled"><a href="#">← Older</a></li>
                    <li className="next"><a href="#">Newer →</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
              </div>
            </div>
          </div>
          {/* Indicators
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="indicators">Indicators</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h2>Alerts</h2>
                <div className="bs-component">
                  <div className="alert alert-dismissible alert-warning">
                    <button type="button" className="close" data-dismiss="alert">×</button>
                    <h4>Warning!</h4>
                    <p>Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, <a href="#" className="alert-link">vel scelerisque nisl consectetur et</a>.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="alert alert-dismissible alert-danger">
                    <button type="button" className="close" data-dismiss="alert">×</button>
                    <strong>Oh snap!</strong> <a href="#" className="alert-link">Change a few things up</a> and try submitting again.
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="alert alert-dismissible alert-success">
                    <button type="button" className="close" data-dismiss="alert">×</button>
                    <strong>Well done!</strong> You successfully read <a href="#" className="alert-link">this important alert message</a>.
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="alert alert-dismissible alert-info">
                    <button type="button" className="close" data-dismiss="alert">×</button>
                    <strong>Heads up!</strong> This <a href="#" className="alert-link">alert needs your attention</a>, but it's not super important.
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <h2>Labels</h2>
                <div className="bs-component" style={{marginBottom: 40}}>
                  <span className="label label-default">Default</span>
                  <span className="label label-primary">Primary</span>
                  <span className="label label-success">Success</span>
                  <span className="label label-warning">Warning</span>
                  <span className="label label-danger">Danger</span>
                  <span className="label label-info">Info</span>
                </div>
              </div>
              <div className="col-lg-4">
                <h2>Badges</h2>
                <div className="bs-component">
                  <ul className="nav nav-pills">
                    <li className="active"><a href="#">Home <span className="badge">42</span></a></li>
                    <li><a href="#">Profile <span className="badge" /></a></li>
                    <li><a href="#">Messages <span className="badge">3</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          {/* Progress bars
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="progress-bars">Progress bars</h1>
                </div>
                <h3 id="progress-basic">Basic</h3>
                <div className="bs-component">
                  <div className="progress">
                    <div className="progress-bar" style={{width: '60%'}} />
                  </div>
                </div>
                <h3 id="progress-alternatives">Contextual alternatives</h3>
                <div className="bs-component">
                  <div className="progress">
                    <div className="progress-bar progress-bar-info" style={{width: '20%'}} />
                  </div>
                  <div className="progress">
                    <div className="progress-bar progress-bar-success" style={{width: '40%'}} />
                  </div>
                  <div className="progress">
                    <div className="progress-bar progress-bar-warning" style={{width: '60%'}} />
                  </div>
                  <div className="progress">
                    <div className="progress-bar progress-bar-danger" style={{width: '80%'}} />
                  </div>
                </div>
                <h3 id="progress-striped">Striped</h3>
                <div className="bs-component">
                  <div className="progress progress-striped">
                    <div className="progress-bar progress-bar-info" style={{width: '20%'}} />
                  </div>
                  <div className="progress progress-striped">
                    <div className="progress-bar progress-bar-success" style={{width: '40%'}} />
                  </div>
                  <div className="progress progress-striped">
                    <div className="progress-bar progress-bar-warning" style={{width: '60%'}} />
                  </div>
                  <div className="progress progress-striped">
                    <div className="progress-bar progress-bar-danger" style={{width: '80%'}} />
                  </div>
                </div>
                <h3 id="progress-animated">Animated</h3>
                <div className="bs-component">
                  <div className="progress progress-striped active">
                    <div className="progress-bar" style={{width: '45%'}} />
                  </div>
                </div>
                <h3 id="progress-stacked">Stacked</h3>
                <div className="bs-component">
                  <div className="progress">
                    <div className="progress-bar progress-bar-success" style={{width: '35%'}} />
                    <div className="progress-bar progress-bar-warning" style={{width: '20%'}} />
                    <div className="progress-bar progress-bar-danger" style={{width: '10%'}} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Containers
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="containers">Containers</h1>
                </div>
                <div className="bs-component">
                  <div className="jumbotron">
                    <h1>Jumbotron</h1>
                    <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                    <p><a className="btn btn-primary btn-lg">Learn more</a></p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h2>List groups</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="bs-component">
                  <ul className="list-group">
                    <li className="list-group-item">
                      <span className="badge">14</span>
                      Cras justo odio
                    </li>
                    <li className="list-group-item">
                      <span className="badge">2</span>
                      Dapibus ac facilisis in
                    </li>
                    <li className="list-group-item">
                      <span className="badge">1</span>
                      Morbi leo risus
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="list-group">
                    <a href="#" className="list-group-item active">
                      Cras justo odio
                    </a>
                    <a href="#" className="list-group-item">Dapibus ac facilisis in
                    </a>
                    <a href="#" className="list-group-item">Morbi leo risus
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="list-group">
                    <a href="#" className="list-group-item">
                      <h4 className="list-group-item-heading">List group item heading</h4>
                      <p className="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                    </a>
                    <a href="#" className="list-group-item">
                      <h4 className="list-group-item-heading">List group item heading</h4>
                      <p className="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h2>Panels</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="panel panel-default">
                    <div className="panel-body">
                      Basic panel
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">Panel heading</div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-body">
                      Panel content
                    </div>
                    <div className="panel-footer">Panel footer</div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="panel panel-primary">
                    <div className="panel-heading">
                      <h3 className="panel-title">Panel primary</h3>
                    </div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      <h3 className="panel-title">Panel success</h3>
                    </div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                  <div className="panel panel-warning">
                    <div className="panel-heading">
                      <h3 className="panel-title">Panel warning</h3>
                    </div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="panel panel-danger">
                    <div className="panel-heading">
                      <h3 className="panel-title">Panel danger</h3>
                    </div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h3 className="panel-title">Panel info</h3>
                    </div>
                    <div className="panel-body">
                      Panel content
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h2>Wells</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="well">
                    Look, I'm in a well!
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="well well-sm">
                    Look, I'm in a small well!
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="bs-component">
                  <div className="well well-lg">
                    Look, I'm in a large well!
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Dialogs
           ================================================== */}
          <div className="bs-docs-section">
            <div className="row">
              <div className="col-lg-12">
                <div className="page-header">
                  <h1 id="dialogs">Dialogs</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6">
                <h2>Modals</h2>
                <div className="bs-component">
                  <div className="modal">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                          <h4 className="modal-title">Modal title</h4>
                        </div>
                        <div className="modal-body">
                          <p>One fine body…</p>
                        </div>
                        <div className="modal-footer">
                          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6">
                <h2>Popovers</h2>
                <div className="bs-component">
                  <button type="button" className="btn btn-default" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">Left</button>
                  <button type="button" className="btn btn-default" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">Top</button>
                  <button type="button" className="btn btn-default" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
            sagittis lacus vel augue laoreet rutrum faucibus.">Bottom</button>
                  <button type="button" className="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">Right</button>
                </div>
                <h2>Tooltips</h2>
                <div className="bs-component">
                  <button type="button" className="btn btn-default" data-toggle="tooltip" data-placement="left" title data-original-title="Tooltip on left">Left</button>
                  <button type="button" className="btn btn-default" data-toggle="tooltip" data-placement="top" title data-original-title="Tooltip on top">Top</button>
                  <button type="button" className="btn btn-default" data-toggle="tooltip" data-placement="bottom" title data-original-title="Tooltip on bottom">Bottom</button>
                  <button type="button" className="btn btn-default" data-toggle="tooltip" data-placement="right" title data-original-title="Tooltip on right">Right</button>
                </div>
              </div>
            </div>
          </div>
          <div id="source-modal" className="modal fade">
            <div className="modal-dialog modal-lg">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title">Source Code</h4>
                </div>
                <div className="modal-body">
                  <pre />
                </div>
              </div>
            </div>
          </div>
          <footer>
            <div className="row">
              <div className="col-lg-12">
                <ul className="list-unstyled">
                  <li className="pull-right"><a href="#top">Back to top</a></li>
                  <li><a href="http://news.bootswatch.com" onclick="pageTracker._link(this.href); return false;">Blog</a></li>
                  <li><a href="http://feeds.feedburner.com/bootswatch">RSS</a></li>
                  <li><a href="https://twitter.com/bootswatch">Twitter</a></li>
                  <li><a href="https://github.com/thomaspark/bootswatch/">GitHub</a></li>
                  <li><a href="../help/#api">API</a></li>
                  <li><a href="../help/#support">Support</a></li>
                </ul>
                <p>Made by <a href="http://thomaspark.co" rel="nofollow">Thomas Park</a>. Contact him at <a href="/cdn-cgi/l/email-protection#2054484f4d415360424f4f545357415443480e434f4d"><span className="__cf_email__" data-cfemail="aadec2c5c7cbd9eac8c5c5ded9ddcbdec9c284c9c5c7">[email&nbsp;protected]</span></a>.</p>
                <p>Code released under the <a href="https://github.com/thomaspark/bootswatch/blob/gh-pages/LICENSE">MIT License</a>.</p>
                <p>Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>. Icons from <a href="http://fortawesome.github.io/Font-Awesome/" rel="nofollow">Font Awesome</a>. Web fonts from <a href="http://www.google.com/webfonts" rel="nofollow">Google</a>.</p>
              </div>
            </div>
          </footer>
        </div>
      </div>
    )
  }
}

export default Theme
